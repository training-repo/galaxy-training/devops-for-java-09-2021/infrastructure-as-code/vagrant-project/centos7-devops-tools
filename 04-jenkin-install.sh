#!/bin/bash
echo "Start script execution---->>04-jenkin-install.sh<----"

echo "Step 1: Install Java OpenJDK 1.8"
sudo yum install -y java-1.8.0-openjdk-devel
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk
export JRE_HOME=/usr/lib/jvm/java-1.8.0-openjdk/jre
export PATH=$PATH:$JAVA_HOME:$JRE_HOME/bin

echo $PATH
echo $JAVA_HOME
echo $JRE_HOME

echo "Step 2: Install Jenkins"
echo "Start by importing the repository key from Jenkins."
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo

echo "After importing the key, add the repository to the system."
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

echo "You can now start Jenkins service using:"
sudo yum install -y jenkins
export JENKINS_HOME=/var/lib/jenkins
export PATH=$PATH:$JENKINS_HOME
echo $JENKINS_HOME

#echo "jenkins   ALL=(ALL)   NOPASSWD:ALL" >> /etc/sudoers

echo "You can now start Jenkins service using:"
sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl status jenkins

echo "Waiting 1 minute ..."
sleep 1m  
echo "all Done."

echo "Print the password on your terminal:"
JENKINSPWD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
echo "The JENKINSPWD is : " $JENKINSPWD

#JENKINS_USERNAME="admin"
#JENKINS_PASSWORD="admin123"

echo "End script execution---->>04-jenkin-install.sh<<----"