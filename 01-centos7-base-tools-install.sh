#!/bin/bash
echo "Start script execution---->>01-centos7-base-tools-install.sh<----"

echo "Update your CentOS 7 system."
sudo yum -y update

echo "Installing epel-release"
sudo yum install -y epel-release

echo "Installing net-tools"
sudo yum install -y net-tools

echo "Installing wget"
sudo yum install -y wget

echo "Installing nano"
sudo yum install -y nano

echo "Installing zip unzip"
sudo yum -y install zip unzip

echo "End script execution---->>01-centos7-base-tools-install.sh<<----"